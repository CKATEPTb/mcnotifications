package bd.CKATEPTb.notifications.gui;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import org.lwjgl.opengl.GL11;

import bd.CKATEPTb.notifications.model.Notification;

public class NotificationGui {
	private static NotificationGui instance = new NotificationGui();
	private List<Notification> notificationList = new ArrayList<Notification>();

	public void drawList() {
		try {
			Iterator<Notification> iterator = this.notificationList.iterator();

			Notification notification;
			for (int translateY = 0; iterator.hasNext(); translateY += 4 + notification.getArea().getHeight()) {
				notification = (Notification) iterator.next();
				if (notification.isShowed()) {
					iterator.remove();
				}

				GL11.glTranslatef(0.0F, (float) translateY, 0.0F);
				notification.draw();
				GL11.glTranslatef(0.0F, (float) (-translateY), 0.0F);
			}
		} catch (ConcurrentModificationException ex) {
		}
	}

	public static NotificationGui getInstance() {
		return instance;
	}

	public List<Notification> getNotificationList() {
		return this.notificationList;
	}
}
