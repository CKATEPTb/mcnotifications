package bd.CKATEPTb.notifications.task;

import java.util.List;
import java.util.Random;
import java.util.TimerTask;

import bd.CKATEPTb.notifications.NotificationsMod;
import bd.CKATEPTb.notifications.network.NotificationMessage;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class NotificationTask extends TimerTask {
	public void run() {
		if (FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getCurrentPlayerCount() > 0) {
			Random random = new Random();
			List<String> msg = NotificationsMod.getMod().getConfig().getMessages();
			for (int i = 0; i < msg.size(); i++) {
				String[] args = msg.get(i).split(":");
				String mes = args[0] + ":" + args[1] + ":" + args[2];
				if (args.length > 3) {
					for (int l = 3; l < args.length; l++) {
						mes = mes + "1CKATEPTb;bTPETAKC1" + args[l];
					}
				}
				msg.set(i, mes);
			}
			List<String> messages = msg;
			String message = (String) messages.get(random.nextInt(messages.size()));
			NotificationsMod.network.sendToAll(new NotificationMessage(message));
		}

	}
}
