package bd.CKATEPTb.notifications.commands;

import org.apache.logging.log4j.Level;

import com.mojang.realmsclient.gui.ChatFormatting;

import bd.CKATEPTb.notifications.NotificationsMod;
import bd.CKATEPTb.notifications.network.NotificationMessage;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class CommandNotifications extends CommandBase {
	public final String NAME = "brc";

	public String getCommandName() {
		return this.NAME;
	}

	@Override
	public String getName() {
		return this.NAME;
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/brc reload - Reload messages.\n" + "/brc [W/S/I] [time(in second)] [message] - Send message";
	}

	@SuppressWarnings("deprecation")
	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 1) {
			String letterType = args[0];
			if ("reload".equalsIgnoreCase(letterType)) {
				NotificationsMod.getMod().getConfig().launch();
				sender.sendMessage(new TextComponentString("§aКонфигурация перезапущена."));
				return;
			}
		}
		if (args.length >= 3
				&& (args[0].equalsIgnoreCase("W") 
						|| args[0].equalsIgnoreCase("S") 
						|| args[0].equalsIgnoreCase("I"))) {
			
			String letterType = args[0];
			
			int delay;
			
			try {
				delay = Integer.parseInt(args[1]);
			} catch (Throwable ex) {
				FMLCommonHandler.instance().getFMLLogger().log(Level.WARN, args[1] + " is not number, set default - 5.");
				delay = 5;
			}
			
			StringBuilder messageText = new StringBuilder();
			
			for (int i = 2; i < args.length; ++i) {
				messageText.append(args[i]).append(" ");
			}

			String message = letterType + ":" + delay + ":" + messageText.toString().replaceAll(":", "1CKATEPTb;bTPETAKC1").trim();
			NotificationsMod.network.sendToAll(new NotificationMessage(message));
			return;
		}
		sender.sendMessage(new TextComponentString(ChatFormatting.DARK_RED + "/brc reload - Reload messages.\n" + ChatFormatting.DARK_RED + "/brc [W/S/I] [time(in second)] [message] - Send message"));
	}
}
