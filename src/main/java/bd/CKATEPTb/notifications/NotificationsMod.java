package bd.CKATEPTb.notifications;

import java.io.File;
import java.util.Timer;

import bd.CKATEPTb.notifications.commands.CommandNotifications;
import bd.CKATEPTb.notifications.config.Config;
import bd.CKATEPTb.notifications.event.RenderEvent;
import bd.CKATEPTb.notifications.network.NotificationMessage;
import bd.CKATEPTb.notifications.network.NotificationPluginMessage;
import bd.CKATEPTb.notifications.task.NotificationTask;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartedEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = "mcnotification", name = "MCNotification", version = "2.0.0")
public class NotificationsMod {
	@Instance
	private static NotificationsMod mod;
	private Config config;
	private File configFile;
	public static SimpleNetworkWrapper network;

	static {
		network = new SimpleNetworkWrapper("CKATEPTb|BRC");
	}

	@EventHandler
	public void onPreInit(FMLPreInitializationEvent event) {
		this.initModules(event.getSuggestedConfigurationFile());
		if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
			MinecraftForge.EVENT_BUS.register(new RenderEvent());
		}
	}

	private void initModules(File file) {
		this.configFile = file;
		network.registerMessage(NotificationMessage.class, NotificationMessage.class, 0, Side.CLIENT);
		//#if SERVER
		network.registerMessage(NotificationPluginMessage.class, NotificationPluginMessage.class, 1, Side.SERVER);
		//#endif
	}

	//#if SERVER
	@EventHandler
	public void onServerStarting(FMLServerStartingEvent event) {
		event.registerServerCommand(new CommandNotifications());
	}

	@EventHandler
	public void onServerStarted(FMLServerStartedEvent event) {
		this.config = new Config(this.configFile);
		this.config.launch();
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new NotificationTask(), 0L, (long) (getMod().getConfig().getDelay() * 1000));
	}
	//#endif

	public static NotificationsMod getMod() {
		return mod;
	}

	public Config getConfig() {
		return this.config;
	}
}
