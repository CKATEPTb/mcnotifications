package bd.CKATEPTb.notifications.model;

import java.util.List;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.Color;

import bd.CKATEPTb.notifications.event.RenderEvent;
import bd.CKATEPTb.notifications.render.Area;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;

public class Notification {
   private int translateX;
   private Area area;
   private long lifetime;
   private long lifestart;
   private Notification.NotificationType type;
   private List<String> message;
   private boolean showed;

   public Notification(String typeLetter, String message, int lifetime) {
      Minecraft minecraft = Minecraft.getMinecraft();
      FontRenderer fonts = minecraft.fontRenderer;
      ScaledResolution resolution = new ScaledResolution(minecraft);
      this.type = Notification.NotificationType.getTypeByLetter(typeLetter);
      this.message = fonts.listFormattedStringToWidth(message, 118);
      this.lifetime = (long)(lifetime * 1000);
      this.lifestart = System.currentTimeMillis();
      int height = (fonts.FONT_HEIGHT + 2) * this.message.size() + 4 > 20?(fonts.FONT_HEIGHT + 2) * this.message.size() + 4:20;
      this.area = new Area(resolution.getScaledWidth(), 8, 144, height);
   }

   public void draw() {
      if(RenderEvent.isMinecraftResized()) {
         Minecraft minecraft = Minecraft.getMinecraft();
         ScaledResolution resolution = new ScaledResolution(minecraft);
         this.area = new Area(resolution.getScaledWidth(), this.area.getY(), this.area.getWidth(), this.area.getHeight());
      }

      if(System.currentTimeMillis() > this.lifetime + this.lifestart) {
         if(this.translateX > 0) {
            this.translateX -= 4;
            this.drawNote();
         } else {
            this.showed = true;
         }
      } else {
         if(this.translateX < 144) {
            this.translateX += 4;
         }

         this.drawNote();
      }

   }

   private void drawNote() {
      int color = Integer.MIN_VALUE;
      GL11.glTranslatef((float)(-this.translateX), 0.0F, 0.0F);
      Gui.drawRect(this.area.getX(), this.area.getY(), this.area.getWidth() + this.area.getX(), this.area.getHeight() + this.area.getY(), color);
      Gui.drawRect(this.area.getX(), this.area.getY(), this.area.getX() + 4, this.area.getHeight() + this.area.getY(), this.type.getColorInt());
      GL11.glEnable('耺');
      RenderHelper.enableGUIStandardItemLighting();
      Minecraft.getMinecraft().getRenderItem().renderItemAndEffectIntoGUI(this.type.itemStack, this.area.getX() + 6, this.area.getY() + this.area.getHeight() / 2 - 8);
      Minecraft.getMinecraft().getRenderItem().renderItemOverlayIntoGUI(Minecraft.getMinecraft().fontRenderer, this.type.itemStack, this.area.getX() + 6, this.area.getY() + this.area.getHeight() / 2 - 8, null);
      RenderHelper.disableStandardItemLighting();
      GL11.glDisable('耺');

      for(int i = 0; i < this.message.size(); ++i) {
         String s = (String)this.message.get(i);
         int j = 0;
         if(this.area.getHeight() == 20) {
            j += 3;
         }

         Minecraft.getMinecraft().fontRenderer.drawStringWithShadow(s, this.area.getX() + 24, 2 + (i + 1) * (Minecraft.getMinecraft().fontRenderer.FONT_HEIGHT + 2) + j, 16777215);
      }

      GL11.glTranslatef((float)this.translateX, 0.0F, 0.0F);
   }

   public Area getArea() {
      return this.area;
   }

   public boolean isShowed() {
      return this.showed;
   }

   public static enum NotificationType {
      WARNING(new ItemStack(Items.LAVA_BUCKET), new Color(204, 32, 2, 140)),
      INFO(new ItemStack(Items.FEATHER), new Color(8, 149, 214, 140)),
      SERVER(new ItemStack(Items.EMERALD), new Color(83, 63, 175, 140)),
      SALE(new ItemStack(Items.DIAMOND), new Color(156, 20, 224, 140));

      private ItemStack itemStack;
      private Color color;

      private NotificationType(ItemStack itemStack, Color color) {
         this.itemStack = itemStack;
         this.color = color;
      }

      public int getColorInt() {
         return (this.color.getAlpha() << 24) + (this.color.getRed() << 16) + (this.color.getGreen() << 8) + this.color.getBlue();
      }

      public static Notification.NotificationType getTypeByLetter(String letter) {
         return letter.equalsIgnoreCase("W")?WARNING:(letter.equalsIgnoreCase("S")?SALE:(letter.equalsIgnoreCase("B")?SERVER:INFO));
      }

      public ItemStack getItemStack() {
         return this.itemStack;
      }

      public Color getColor() {
         return this.color;
      }
   }
}
