package bd.CKATEPTb.notifications.network;

import bd.CKATEPTb.notifications.gui.NotificationGui;
import bd.CKATEPTb.notifications.model.Notification;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class NotificationPluginMessage implements IMessage, IMessageHandler<NotificationPluginMessage, NotificationPluginMessage> {
   private String text;
   public NotificationPluginMessage tests;

   public NotificationPluginMessage() {
   }

   public NotificationPluginMessage(String text) {
      this.text = text;
   }

   public void fromBytes(ByteBuf buf) {
      this.text = ByteBufUtils.readUTF8String(buf).substring(1);
   }

   public void toBytes(ByteBuf buf) {
      ByteBufUtils.writeUTF8String(buf, this.text);
   }

   public NotificationPluginMessage onMessage(NotificationPluginMessage message, MessageContext ctx) {
      String messageText = message.text;
      Notification model = new Notification("B", messageText, 10);
      NotificationGui.getInstance().getNotificationList().add(model);
      return null;
   }

}

