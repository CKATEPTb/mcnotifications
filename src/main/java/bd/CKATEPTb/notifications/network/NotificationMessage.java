package bd.CKATEPTb.notifications.network;

import org.apache.logging.log4j.Level;

import bd.CKATEPTb.notifications.gui.NotificationGui;
import bd.CKATEPTb.notifications.model.Notification;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class NotificationMessage implements IMessage, IMessageHandler<NotificationMessage, NotificationMessage> {
	private String text;

	public NotificationMessage() {
	}

	public NotificationMessage(String text) {
		this.text = text;
	}

	public void fromBytes(ByteBuf buf) {
		this.text = ByteBufUtils.readUTF8String(buf);
	}

	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, this.text);
	}

	@SuppressWarnings("deprecation")
	public NotificationMessage onMessage(NotificationMessage message, MessageContext ctx) {
		String[] args = message.text.split(":");
		String letterType = args[0];
		int lifetime;
		try {
			lifetime = Integer.parseInt(args[1]);
		} catch (Throwable ex) {
			FMLCommonHandler.instance().getFMLLogger().log(Level.WARN, "Null number, set default - 5.");
			lifetime = 5;
		}
		if (args.length >= 3) {
			String messageText = args[2];
			Notification model = new Notification(letterType, messageText.replaceAll("1CKATEPTb;bTPETAKC1", ":").replaceAll("&", "§"),
					lifetime);
			NotificationGui.getInstance().getNotificationList().add(model);
		}
		return null;
	}
}
