package bd.CKATEPTb.notifications.event;

import bd.CKATEPTb.notifications.gui.NotificationGui;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.client.event.RenderGameOverlayEvent.Post;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class RenderEvent {
   private static boolean minecraftResized = false;
   private static int oldWidth;
   private static int oldHeight;

   @SubscribeEvent
   @SideOnly(Side.CLIENT)
   public void onRender(Post event) {
      if(event.getType() == ElementType.ALL) {
         if(oldWidth == event.getResolution().getScaledWidth() && oldHeight == event.getResolution().getScaledHeight()) {
            minecraftResized = false;
         } else {
            oldWidth = event.getResolution().getScaledWidth();
            oldHeight = event.getResolution().getScaledHeight();
            minecraftResized = true;
         }
         NotificationGui.getInstance().drawList();
      }

   }

   public static boolean isMinecraftResized() {
      return minecraftResized;
   }
}
